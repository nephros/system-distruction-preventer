Name:           system-distruction-preventer
Version:        3.3
Release:        3%{?dist}
Summary:        This RPM blocks others which should not be installed.
URL:            https://gitlab.com/nephros/system-distruction-preventer
License:        GPLv2

Source:         system-distruction-preventer-3.3.tar
BuildArch:      noarch

Requires: libhybris-libEGL
Requires: libhybris-libGLESv2
Conflicts: mesa-llvmpipe-libEGL
Conflicts: mesa-llvmpipe-libGLESv1
Conflicts: mesa-llvmpipe-libGLESv2

%description
An attempt to avoid the unstable state caused by some development packages. See also:
https://together.jolla.com/question/229060/hybrismesa-package-replacement-maybe-screwed-up-my-system/
https://forum.sailfishos.org/t/installing-and-uninstalling-hybris-mesa-packages-kills-device/2010

%preun
for f in  %{_libdir}/libGLES* %{_libdir}/libEGL*; do
cp -u -d "$f" "${f}_sdpbackup" ||:
done ||:
# this will cause uninstallation to fail. This is intentional.
# see the README
/bin/false

%files

%changelog
* Tue Aug 31 2021 Nephros <sailfish@nephros.org> 3.3-3
- loop in the preun script, should catch all libraries
* Tue Sep  8 2020 Nephros <sailfish@nephros.org> 3.3-2
- cause uninstallation to backup the libraries and always fail in the pre-uninstall script
* Tue Sep  8 2020 Nephros <sailfish@nephros.org> 3.3-1
- first version
