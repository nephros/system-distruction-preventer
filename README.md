# SailfishOS System Distruction Preventer

**NOTE**: recent versions of SFOS will not actually allow the dangrous packages
to be installed. So this package is more or less deprecated now.

This is a simple little RPM file which was made at an attempt to guard against
the following scenario:

## The Problem

On SailfishOS, the two critical system libraries `/usr/lib/libEGL.so.1` and
`/usr/lib/libGLESv2.so.2`, (and possibly others), are provided by two different
sets of RPM packages.

One set comes with the base installation (libhybris-libEGL and
libhybris-libGLESv2), another set (mesa-llvmpipe-libEGL and
mesa-llvmpipe-libGLESv2) may be installed as dependencies of certain
development packages, needed to compile applications directly on the device.

A problem can arise when installing the second set, which will overwrite the
mentioned library files, and then uninstalling them again.  
Uninstalling will remove the libraries (as they are owned by the mesa-llvmpipe
RPM), resulting in a system where the UI and any applications can not start.

When the system is in this state, it may/will not boot into the graphic
environment, so it's essentially bricked.  
Apparently, the packaging tools also lose the ability to authenticate against
the official SailfishOS/Jolla repositories, to the situation can not easily be
fixed again by reinstalling the missing RPM files.

## The (attempt at) a solution:

It's a simple RPM which conflicts with the dangerous ones. It does not install any files.
After installing this, `rpm`, `pkcon`, and `zypper` should at least warn you about the conflict.

Current versions of the RPM will, in addition to the conflict:

 - a) back up the libraries in `/usr/lib`
 - b) intentionally fail the `preun` macro in the RPM, so uninstallation should fail. To really uninstall use `rpm -e --noscript`.

Review the .spec file in this repo for details.

Do remember to **uninstall it before updating your OS**, to avoid problems because of the intentional failure above.

## If you are affected by this problem:

I have found a quite complicated remedy, see the references below.

**It is important** if you notice that applications will not start, that **you do not power off** and **do not reboot** the OS.

Instead, use ssh to connect (either via WiFi or USB), and make sure the
connection stays active. Deactivate any services which may cause the WiFi
connection to be closed.  It would be smart to start a GNU screen session so
you are safe when a connection times out. Unset the TMOUT variable in bash if it is defined.

Using the ssh session, verify that the problem is actually the one described
here.

Then collect the files as mentioned in the linked guide, scp them over to the
device and put them in the right place.

### References:

- https://together.jolla.com/question/229060/hybrismesa-package-replacement-maybe-screwed-up-my-system/
- https://forum.sailfishos.org/t/installing-and-uninstalling-hybris-mesa-packages-kills-device/2010

